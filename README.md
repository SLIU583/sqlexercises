# SQLExercises

 Lynda: SQL Tips, Tricks & Techniques exercise files 
 
 1. 04_01.sql
 2. 05_01.sql
 3. 06_01.sql
 4. 07_01.sql
 5. 08_01.sql
 6. 09_01.sql
 7. 10_01.sql
 8. 11_01.sql
 9. 12_01.sql
 

Start with importing AdventureWorks database. The tutorial is many years ago and
result in several bugs. New tutorials for reference as below:

4/15/2019

Source Files:

* /DLLConstructs/

* /DataInserts/

* AdvWorks_Create_User.sql

* AdvWorks_Build_Data.sql


Instructions (Windows):
-----------------------------------------------
1. 	Open Command-Line Prompt 

2. 	Change the directory to the directory under which you have unzipped the 
	download 
  
    (ex: cd c:\downloads\oracle_essbase_studio_tutorial\). 

3.	Enter SQLPLUS in the command-line to start SQL*PLUS.

	
	EXAMPLE:
	C:\downloads\oracle_essbase_studio_tutorial> sqlplus /nolog


4.	Connect using a SYSDBA account 

	
    EXAMPLE:
    ```SQL> Connect system as sysdba```


     ...(when prompted for the password, enter the password, and hit the enter key)
  
      Forgot password or the default password does not work: https://stackoverflow.com/questions/740119/default-passwords-of-oracle-11g
  
  

5.	Run the initial user/scheme creation and priviliges script by entering the following in the command-line:


	`SQL> @AdvWorks_Create_User_Script.sql;`

	
	Press the Enter key to Execute the script. The script is completed if the message "SCRIPT COMPLETE!!" being displayed 


6.	In Step 5 the user scheme was created along 
	with its privileges (Privileges are for example only and do not mimic a production environment).

	Enter the following in the SQLPLUS command-line to build the table objects and load data:


	`SQL> @AdvWorks_Build_Data.sql;`

